import { RenderRoutes } from './_utils';

import generalRoutes from './general';

const ROUTES = [
  generalRoutes,
];

export { RenderRoutes };
export default ROUTES;
