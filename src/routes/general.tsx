import { GeneralScreen } from '../screens';

export default {
  path: '/',
  key: 'ROOT',
  exact: true,
  component: GeneralScreen,
};
