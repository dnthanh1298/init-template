import React, { useState, useEffect } from 'react';

import DataView from '../../../components/GeneralStatisticsDataView';

const data = [
  { text: 'hello' },
  { text: 'hi' },
  { text: 'hehe' },
];

interface Statistic {
  text: string
}

export default function StatisticPreivew() {
  const [statistics, setStatistics] = useState<Statistic[]>([]);

  useEffect(() => {
    setStatistics(data);
  }, []);

  const listDataViews = statistics.map((d) => <DataView text={d.text} />);

  return (
    <>
      {listDataViews}
    </>
  );
}
