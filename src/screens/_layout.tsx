import React from 'react';

interface Props {
  children: React.ReactNode
}

export default function Layout(props: Props) {
  return (
    <>
      <div>
        <h1>Layout</h1>
        {props.children}
      </div>
    </>
  );
}
