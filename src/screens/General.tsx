import React from 'react';

import { StatisticPreivews } from '../features/General/containers';
import Layout from './_layout';

export default function GeneralScreen() {
  return (
    <>
      <Layout>
        <StatisticPreivews />
      </Layout>
    </>
  );
}
