import React from 'react';

import CardWrapper from '../shared/CardWrapper';

interface Props {
  text: string
}

export default function GeneralStatisticDataView(props: Props) {
  return (
    <>
      <CardWrapper>
        <h1>{props.text}</h1>
      </CardWrapper>
    </>
  );
}
