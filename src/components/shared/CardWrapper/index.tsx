import React from 'react';

import useStyle from './style';

interface Props {
  children: React.ReactNode
}

export default function CardWrapper(props: Props) {
  const classes = useStyle();

  return (
    <>
      <div className={classes.cardWrapper}>
        {props.children}
      </div>
    </>
  );
}
